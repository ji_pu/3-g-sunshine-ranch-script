# 3G阳光牧场_脚本

#### 介绍
nodejs实现,可使用青龙面板定期执行,自动化完成

#### 软件架构
nodejs
npm

#### 使用说明

1. 手动运行

   2. 不编译使用

      1. 下载仓库
      2. 解压仓库到目标文件夹
      3. 运行 `npm run dev` 或 `node index.js`
   3. 编译使用
      1. 下载仓库
      2. 需要NPM, 后运行: `npm install esbuild -g`
      3. 在仓库根目录运行 `npm run build`
      4. 运行 `npm run start`
   
2. 青龙面板

4. 安装前置库 [sync-request](https://www.npmjs.com/package/sync-request) [crypto](https://www.npmjs.com/package/crypto-js) [better-sqlite3](https://www.npmjs.com/package/better-sqlite3)

      > ```shell
      > npm install sync-request crypto-js better-sqlite3
      > ```
      
5. 运行 index.js


#### 功能
1. 每日签到
   >  签到成功后会向[pushMe](https://push.i-i.me/)推送日志,签到不成功不推送

2. 自动一键采摘

   > 自动收获已经成熟的作物

3. 自动一键铲地

   > 自动将地块变为可种植

4. 自动种植

   > 自动种植植物 (需背包有种子)

5. 自动一键操作

   > 自动使用一键操作

6. 自动挖宝

   > 当没有种子时, 自动挖宝获得种子
   >
   > 如果没有宝藏地图则自动合成
   >
   > 如果无法合成宝藏地图则自动购买宝藏地图
   >
   > 优先购买高级宝藏地图, 钱不够则购买普通宝藏地图
   >
   > 要是你连这钱都没有, 那就让地空着

7. 自动领取活动奖励

   > 会在每日23:50后尝试领取每日礼包和奖励

8. 自动制标

   > 会在每日23:50后查看是否有可制标的植物,并尝试制标

9. 自动出售植物

   > 会自动出售已经制标的植物

10. 自动施肥

   > 会为范围内的植物自动施肥,是否购买肥料来施肥更具配置

11. 自动捕捉动物

    > 会自动捕捉来访的动物,如果没有诱捕球则自动购买普通诱捕球

11. 自动补充饲料

    > 在配置文件中设定是否使用精饲料

11. 自动收获动物

    > 自动判断并进行收获

11. 一键操作动物

    > 对动物使用一键操作

11. 自动饲养动物

11. 自动购买动物

    > 随机在商店购买动物

11. 自动出售动物副产品

11. 日志

   >  每次运行日志保存在log文件夹下
   >  日志内容推送至[pushMe](https://push.i-i.me/) (内置pushMe SDK,无需调用青龙功能)

#### 配置

配置文件为项目目录下的 config.js 文件,需要手动将 config.js.d 配置模板文件重命名为 config.js ,后项目才能正常运行

1.  `user_env`:
   1.  `user_id` 你抓包获得的userId
   1.  `usedelicate`  是否使用精致饲料
   1.  `buyFertilizer` 是否购买肥料

1.  `push_config` :
   1.  `PUSHME_KEY` :  [pushMe](https://push.i-i.me/) 手机端获取的key
   2.  `PUSHME_URL` : [pushMe](https://push.i-i.me/) 的服务器,不填则使用默认服务器
2.  `log` : 
   1.  `saveTime` :  运行日志保存的时间,单位天

