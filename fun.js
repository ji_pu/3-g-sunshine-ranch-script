/*
 * @Date: 2024-06-24 09:38:34
 */

const Front = require('./Front.js');
module.exports = class extends Front {
    userId;
    Url = "http://ygmcapi.yiyutx.top/ygmc/";
    constructor(userId) {
        super();
        this.userId = userId;
    }
    getHomeData() {
        let data = this.user_data();
        let res = super.post(this.Url + "home/getHomeData", data);
        delete res.bdcMsgList;
        delete res.noticeData;
        delete res.bomMenuList;
        return res;
    }
    sign(HomeData) {
        if (HomeData.isSign == 0) {
            let data = this.user_data();
            let res = super.post(this.Url + "sign/sign", data);
            return res;
        } else {
            return false;
        }
    }

    harvestAllCrop() {//采摘
        let data = this.user_data({
            "portType": "0",
        });
        let res = super.post(this.Url + "port/harvestAllCrop", data);
        return res;
    }

    getMyKnapsackList() {
        let res = super.post(this.Url + "knapsack/getMyKnapsackList", this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "1",
            "specimen": "-1",
            "rarity": 0
        }));
        if (res.total == 0) return false;
        return res.list[0];
    }

    getMyFertilizerList() {
        let res = super.post(this.Url + "knapsack/getMyKnapsackList", this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "3",
            "rarity": "31"
        }));
        return res.list;
    }

    sow(port, goodsData) {
        let data = this.user_data({
            "portType": "0",
            "portId": port.id,
            "type": "2",
            "goodsId": "" + goodsData.id,
            "rarity": goodsData.rarity
        });
        let res = super.post(this.Url + "port/sow", data);
        return res;
    }

    portOperationAll(type) {
        let data = this.user_data({
            "portId": "0",
            "type": type
        });
        let res = super.post(this.Url + "port/portOperationAll", data);
        return res;
    }

    getTreasureList() {
        let data = this.user_data({
            "type": 1
        });
        let res = super.post(this.Url + "treasure/getTreasureList", data);
        if (res.length == 0) return false;
        let Map = false;
        for (let i = 0; i < res.length; i++) {
            let Treasure = res[i];
            if (Treasure.entry_level <= Treasure.entry_level2 && (!Map || Map.id < Treasure.id)) Map = Treasure;
        }
        return Map;
    }

    getTreasureDetail(Map) {
        let data = this.user_data({
            "id": Map.id
        });
        let res = super.post(this.Url + "treasure/getTreasureDetailList", data);
        for (let i = 0; i < res.prop.length; i++) {
            const prop = res.prop[i];
            if (prop.num > 0) return prop;
        }
        return false;
    }

    getSpecimenList(rarity) {
        let data = this.user_data({
            pageSize: "1",
            pageNum: "10",
            type: "1",
            goodsName: "",
            rarity: rarity,
            ismake: "0"
        });
        let res = super.post(this.Url + "specimen/getSpecimenList", data);
        let items = [];
        if (!res) return items;
        for (let i = 0; i < res.list.length; i++) {
            const item = res.list[i];
            if (!item.goodsState || !item.moneyState) continue;
            items.push(item);
        }
        return items;
    }
    getMakeMsg(id) {
        let data = this.user_data({
            "id": "" + id,
            "type": "2"
        });
        let res = super.post(this.Url + "makeHouse/getMakeMsg", data);
        return (!res) ? false : (() => {
            let list = res.list;
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item.sd == 0) return false;
            }
            return true;
        })();
    }

    receiveAllActiveReward(type) {
        let data = this.user_data({
            "type": type + ""
        });
        let res = super.post(this.Url + "active/receiveAllActiveReward", data);
        return res;
    }
    make(id) {
        let data = this.user_data({
            "id": "" + id,
            "type": "2",
            "num": "1"
        });
        let res = super.post(this.Url + "makeHouse/make", data);
        return res;
    }

    openTreasure(id) {
        let data = this.user_data({
            "id": id,
            "num": 1
        });
        let res = super.post(this.Url + "treasure/openTreasure", data);
        return res;
    }

    shopadd(goodsId) {
        let data = this.user_data({
            "goodsId": goodsId,
            "num": "1",
            "type": "3",
            "rarity": "5"
        });
        let res = super.post(this.Url + "shop/add", data);
        return res;
    }

    makeSpecimen(id) {
        let data = this.user_data({
            specimenId: id
        });
        let res = super.post(this.Url + "specimen/makeSpecimen", data);
        return res;
    }

    getFarmData() {
        let data = this.user_data();
        let res = super.post(this.Url + "home/getFarmData", data);
        return res;
    }

    getMyWarehouseList() {
        let data = this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "1",
            "rarity": "0",
            "specimen": "1",
        });
        let res = super.post(this.Url + "warehouse/getMyWarehouseList", data);
        return (res.total == 0) ? false : res.list;
    }

    sellGoods(good) {
        let data = this.user_data({
            "goodsId": good.id,
            "num": good.warehouseNum,
        });
        let res = super.post(this.Url + "warehouse/sellGoods", data);
        return res;
    }
    buyFertilizer(num = 1) {
        let data = this.user_data({
            "goodsId": 171,
            "num": num,
            "type": "3",
            "rarity": "5"
        });
        let res = super.post(this.Url + "shop/add", data);
        return res;
    }

    fertilize(port, fertilizer, num = 1) {
        let data = this.user_data({
            "portType": "0",
            "type": "6",
            "portId": port.id + "",
            "knapsackId": fertilizer.id + "",
            "usenum": num
        });
        let res = super.post(this.Url + "port/portOperation", data);
        return res;
    }

    getybqList() {
        let data = this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "3",
            "rarity": "39"
        });
        let res = super.post(this.Url + "knapsack/getMyKnapsackList", data);
        return res.list;
    }

    catch(ybq, animal) {
        let data = this.user_data({
            "goodsId": ybq.goods_id,
            "ksId": ybq.knId,
            "visitAnimalId": animal.id + ""
        });
        let res = super.post(this.Url + "animalvisit/catch", data);
        return res;
    }

    buyybq() {
        let data = this.user_data({
            "goodsId": 3604,
            "type": "3",
            "rarity": "5",
            "num": "1"
        });
        let res = super.post(this.Url + "shop/add", data);
        return res;
    }

    getFeedList() {
        let data = this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "3",
            "rarity": "35",
            "feedType": "1",
        });
        let res = super.post(this.Url + "knapsack/getMyKnapsackList", data);
        return res.list;
    }

    useFeed(feed) {
        let data = this.user_data({
            "goodId": feed.goods_id,
            "id": feed.id
        });
        let res = super.post(this.Url + "farm/addFeed", data);
        return res;
    }

    buyFeed(id, num = 1) {
        let data = this.user_data({
            "goodsId": id,
            "num": num,
            "type": "3",
            "rarity": "5"
        });
        let res = super.post(this.Url + "shop/add", data);
        return res;
    }

    harvestAllAnimal() {
        let data = this.user_data();
        let res = super.post(this.Url + "farm/harvestAllAnimal", data);
        return res;
    }

    farmOperationAll() {
        let data = this.user_data({
            type: "1"
        });
        let res = super.post(this.Url + "farm/farmOperationAll", data);
        return res;
    }

    getAnimal() {
        for (let i = 3; i < 6; i++) {
            let data = this.user_data({
                "pageSize": "1",
                "pageNum": "10",
                "type": "2",
                "specimen": "-1",
                "rarity": "" + i,
            });
            let res = super.post(this.Url + "knapsack/getMyKnapsackList", data);
            if (!res) return false;
            if (res.list.length) return res.list[0];
        }
        return false
    }

    getAnimalShopList() {
        let data = this.user_data({
            "pageSize": "1",
            "pageNum": "10",
            "type": "2",
            "itemType": "1",
            "rarity": "5",
            "goodsName": ""
        });
        let res = super.post(this.Url + "shop/get", data);
        return res ? res.list : false;
    }

    buyAnimal(id) {
        let data = this.user_data({
            "goodsId": id,
            "num": "1",
            "type": "2",
            "rarity": "5"
        });
        let res = super.post(this.Url + "shop/add", data);
        return res;
    }

    useAnimal(animal, port) {
        let data = this.user_data({
            "portType": "0",
            "portId": "" + port.id,
            "type": "2",
            "goodsId": "" + animal.goods_id,
            "rarity": "5",
        });
        let res = super.post(this.Url + "farm/raise", data);
        return res;
    }

    getMyAnimal() {
        let ls = [];
        for (let i = 3; i < 6; i++) {
            let data = this.user_data({
                "pageSize": "1",
                "pageNum": "10",
                "type": "21",
                "rarity": ""+i,
                "specimen": "0",
            });
            let res = super.post(this.Url + "warehouse/getMyWarehouseList", data);
            if (res) {
                ls.concat(res.list)
            }
        }
        return (ls.length)?ls:false;
    }

    makeAnimal(id,num){
        let data = this.user_data({
            "goodsId": id,
            "num": ""+num,
        });
        let res = super.post(this.Url + "warehouse/sellGoods", data);
        return res;
    }

    user_data(param = {}) {
        return {
            "data": Object.assign({
                "userId": this.userId,
                "version": "2.6.1"
            }, param),
            "checkTime": new Date().getTime,
            "imei": "",
            "type": 1,
            "version": "2.6.1"
        };
    }
}